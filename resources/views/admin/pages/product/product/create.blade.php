

@extends('admin.base')

@section('content')

    <div class="container-fluid">


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Create Primary Product</h4>
                    </div>
                    <hr>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal"
                          action="{{route('admin.product.primary.save')}}"

                          method="POST" enctype="multipart/form-data"

                    >

                        {{ csrf_field() }}
                        <div class="card-body">
                            <h4 class="card-title">Basic Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control product-name" name="title" placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email1" class="col-sm-3 text-right control-label col-form-label">Meta Description</label>
                                <div class="col-sm-9">
                                    <input type="text" name="meta" class="form-control"  placeholder="Meta Description">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Slug</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control product-slug" readonly name="slug" placeholder="Slug">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">HSN</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="hsn" placeholder="HSN">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Code Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="code_name" placeholder="Code Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Type</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="type" placeholder="Type">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Unit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="unit" placeholder="Unit">
                                </div>
                            </div>
                        </div>
                        <hr>






                        <div class="card-body">
                            <h4 class="card-title">Category & Tags</h4>




                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Primary Category</label>
                                <div class="col-sm-9">
                                    <select name="primary_category" class="form-control primcat">

                                        @foreach($pcs as $pc)




                                        <option value="{{$pc->id}}">{{$pc->title}}</option>


                                            @endforeach

                                    </select>
                                </div>
                            </div>

                            <input class="primarycatname" type="hidden" value="
                            @foreach($pcs as $pc)




                                       {{$pc->title}}

                            @endforeach
                            " name="primary_category_name">
                            <input class="secondarycatname" name="secondary_category_name" type="hidden" value="@foreach($scs as $pc)




                            {{$pc->title}}

                            @endforeach" >


                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Secondary Category</label>
                                <div class="col-sm-9">
                                    <select name="secondary_category" class="form-control seccat">
                                        @foreach($scs as $pc)



                                            <option value="{{$pc->id}}">{{$pc->title}}</option>


                                        @endforeach

                                    </select>
                                </div>
                            </div>









                        </div>


                        <div class="card-body">
                            <h4 class="card-title">Price & Tax</h4>
                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Base Price</label>
                                <div class="col-sm-9">
                                    <input type="text" value="0"
                                           id="product-base-price"
                                           class="form-control product-base-price" name="value" placeholder="Base Price">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Discount</label>
                                <div class="col-sm-9">
                                    <input type="text" VALUE="0"
                                           class="form-control product-discount" name="discount"   placeholder="Discount">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Discount %</label>
                                <div class="col-sm-9">
                                    <input type="text" VALUE="0"
                                           class="form-control product-discount-percentage"
                                           readonly name="discount_per"   placeholder="Discount %">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">After Discount</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control product-after-discount"
                                           name="after_discount_amt" readonly placeholder="After Discount">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Tax %</label>
                                <div class="col-sm-9">
                                    <select class="form-control product-tax-percentage" name="taxper">
                                        <option value="0">Choose Your Option</option>
                                        <option value="0">0%</option>
                                        <option value="5">5%</option>
                                        <option value="12">12%</option>
                                        <option value="18">18%</option>
                                        <option value="28">28%</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="abpro" class="col-sm-3 text-right control-label col-form-label">CGST</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control product-cgst" name="cgst" placeholder="CGST">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="abpro" class="col-sm-3 text-right control-label col-form-label">SGST</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control product-sgst" name="sgst" placeholder="SGST">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="abpro" class="col-sm-3 text-right control-label col-form-label">IGST</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control product-igst" name="igst" placeholder="IGST">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="abpro" class="col-sm-3 text-right control-label col-form-label">TAX AMT</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control product-tax-amt" name="tax_amt" placeholder="TAX AMT">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="abpro" class="col-sm-3 text-right control-label col-form-label">Total</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control product-total-amt" name="total" placeholder="Total">
                                </div>
                            </div>




                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Images SD HD HD+ </h4>
                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Large Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="imglg" class="form-control"  placeholder="Base Price">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Small Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="imgsm" class="form-control"  placeholder="Discount">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="com1" class="col-sm-3 text-right control-label col-form-label">Fab Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="imgssm" class="form-control"  placeholder="After Discount">
                                </div>
                            </div>










                        </div>

                        <hr>

                        <div class="card-body">
                            <h4>Details</h4>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                <div class="col-sm-9">


                                    <textarea id="my-editor" name="description" class="form-control">

                                    </textarea>



                                </div>
                            </div>
                        </div>


                        <hr>










                        <hr>
                        <div class="card-body">
                            <div class="form-group m-b-0 text-right">
                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>





    </div>
    <!-- End Row -->



@endsection


@section('admin_footer')



    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
    </script>

    <script>
        CKEDITOR.replace('my-editor', options);
    </script>


    <script>



        $(document).ready(function(){
            $(".product-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".product-slug").val(actualSlug);



            });

            $(".product-base-price").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


                 $(".product-discount").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


                 $(".product-tax-percentage").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


                 $(".primcat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                     updatehiddenpc();
            }));



                 $(".seccat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                     updatehiddensc();
            }));

                 function updatehiddenpc() {

                     var pccnam =  $(".primcat option:selected").text();

                     $(".primarycatname").val(pccnam);

                 }

                 function updatehiddensc() {

                     var pccnam =  $(".seccat option:selected").text();

                     $(".secondarycatname").val(pccnam);

                 }




            



       //    var CGST =  $(".product-cgst").val();
       //    var SGST =  $(".product-sgst").val();
        //   var IGST =  $(".product-igst").val();
          // var TOTAL =  $(".product-total").val();
        //   var TOTALTAX =  $(".product-tax-amt").val();


            
            function onPriceupdate() {
                var BASEPRICE =  $(".product-base-price").val();
                var TAXPERCENTAGE =  $(".product-tax-percentage option:selected").val();
                var DISCOUNT =  $(".product-discount").val();



                //calcul;ate actual price

                var AFTERDISC = BASEPRICE - DISCOUNT ;

                var DISC_PERCENTAGE = (DISCOUNT/BASEPRICE )*100

                $(".product-after-discount").val(AFTERDISC.toFixed(2));
                $(".product-discount-percentage").val(DISC_PERCENTAGE.toFixed(2));


                //cal gst
                //
                //
                //

                var TocalculatePrice =  $(".product-after-discount").val();


                var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
             var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
             var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
             var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
             var   TOTAL = TocalculatePrice + TOTALTAX;


                $(".product-cgst").val(CGST.toFixed(2));
                $(".product-sgst").val(SGST.toFixed(2));
                $(".product-igst").val(IGST.toFixed(2));
                $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".product-total-amt").val((Number(TocalculatePrice) + Number(TOTALTAX)).toFixed(2));




              //  $(".product-after-discount").val(AFTERDISC);

              //  console.log(BASEPRICE);








                
            }
            









            
            
            
            
            
            
            
            
        });
        
        
        
        


    </script>


@endsection


