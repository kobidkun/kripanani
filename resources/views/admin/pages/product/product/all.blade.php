

@extends('admin.base')

@section('content')

    <div class="container-fluid">


        <div class="row">


            <div class="col-12">


                <table  class="table table-striped table-bordered display" style="width:100%" id="users-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Code Name</th>
                        <th>Type</th>
                        <th>Value</th>
                        <th>Discount</th>
                        <th>Tax %</th>
                        <th>Tax Amt</th>
                        <th>Total</th>
                        <th>Updated At</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                </table>


            </div>


        </div>





    </div>
    <!-- End Row -->



@endsection


@section('admin_footer')




    <link href="{{asset('../admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.product.primary.datatablesapi') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'code_name', name: 'code_name' },
                    { data: 'type', name: 'type' },
                    { data: 'value', name: 'value' },
                    { data: 'discount', name: 'discount' },
                    { data: 'taxper', name: 'taxper' },
                    { data: 'tax_amt', name: 'tax_amt' },
                    { data: 'total', name: 'total' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>


@endsection


