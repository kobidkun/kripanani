

@extends('admin.base')

@section('content')

    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
    <style>
        .adminimages{
            padding-right: 20px;
            width: 250px;
        }
    </style>

    <div class="container-fluid">





        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">{{$p->title}}</h3>

                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="white-box text-center"> <img src="/storage/{{$p->img_hd}}" alt="{{$p->title}}"
                                                                         class="img-responsive adminimages"> </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-6">
                                <h4 class="box-title m-t-40">Product description</h4>
                                <p>
                                    {{$p->description}} <br>
                                    {{$p->size}}

                                    </p>
                                <h2 class="m-t-40">
                                    <i class=" fas fa-rupee-sign"></i>
                                   <strike> {{$p->value}}</strike> <small class="text-success">({{$p->discount_per}}% off)</small>

                                    <i class=" fas fa-rupee-sign"></i>  {{$p->after_discount_amt}}
                                </h2>


                                <button class="btn btn-danger btn-rounded"
                                        data-toggle="modal" data-target=".bs-example-modal-lg"
                                ><i class="fa fa-check"></i> Add Features</button>


                                <h3 class="box-title m-t-40">Key Highlights</h3>
                                <ul class="list-icons">
                                    @foreach($fs as $f)
                                        <li><i class="fa fa-check text-success"></i> {{$f->feature}}</li>
                                    @endforeach






                                </ul>




                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3 class="box-title m-t-40">General Info</h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td width="390">Meta</td>
                                            <td> {{$p->meta}} </td>
                                        </tr>

                                        <tr>
                                            <td>Slug</td>
                                            <td> {{$p->slug}} </td>
                                        </tr>
                                        <tr>
                                            <td>HSN</td>
                                            <td> {{$p->hsn}} </td>
                                        </tr>
                                        <tr>
                                            <td>Code Name</td>
                                            <td> {{$p->code_name}} </td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td> {{$p->type}} </td>
                                        </tr>
                                        <tr>
                                            <td>Unit</td>
                                            <td> {{$p->unit}} </td>
                                        </tr>
                                        <tr>
                                            <td>Value</td>
                                            <td> {{$p->value}} </td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td> {{$p->discount}} </td>
                                        </tr>
                                        <tr>
                                            <td>Tax %</td>
                                            <td> {{$p->taxper}} </td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td> {{$p->total}} </td>
                                        </tr>
                                        <tr>
                                            <td>Is Featured</td>
                                            <td> {{$p->featured}} </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->





            <div class="col-lg-12">
                <div class="card">







                        <form method="post" action="{{route('admin.product.variants.upload.image',$p->id)}}"
                              enctype="multipart/form-data"
                              class="dropzone" id="dropzone">
                            @csrf
                        </form>













                </div>
            </div>









            <div class="col-lg-12">
                <div class="card">
                    <div class="col-lg-12 col-md-12 col-sm-12">





                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Feature</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($fs as $f)

                                    <tr>
                                        <td>{{$f->id}}</td>
                                        <td>{{$f->feature}}</td>
                                        <td>
                                            <a href="" class="btn btn-danger">Delete</a>
                                        </td>


                                    </tr>

                                @endforeach


                                </tbody>
                            </table>

                        </div>











                    </div>
                </div>
            </div>




        </div>





    </div>
    <!-- End Row -->


    {{--modal add features--}}


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <div class="modal-body">
                        <h4>Add Features</h4>

                        <form class="form-horizontal"
                              action="{{route('admin.product.variants.add.feature')}}"

                              method="POST"

                        >

                            {{ csrf_field() }}
                            <div class="card-body">
                                <h4 class="card-title">Feature</h4>
                                <div class="form-group row">
                                    <label for="feature" class="col-sm-3 text-right control-label col-form-label">Feature</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control " name="feature" placeholder="Feature">
                                        <input type="hidden"  name="product_id" value="{{$p->id}}" >
                                    </div>
                                </div>

                                <button class="bt btn-primary" type="submit">Submit</button>




                            </div>
                        </form>
                        <hr>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

@endsection


@section('admin_footer')




    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>



@endsection


