

@extends('admin.base')

@section('content')



        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Primary Category</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Is Featured</th>
                                    </tr>
                                    </thead>
                                    <tbody>



                                    @foreach($categories as $category)


                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->title}}</td>
                                        <td>{{$category->description}}</td>
                                        <td>{{$category->featured}}</td>
                                    </tr>

                                        @endforeach




                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- End Row -->



@endsection


@section('admin_footer')







@endsection


