

@extends('admin.base')

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4 class="card-title text-white">Create Primary Category</h4>
                    </div>
                    <form action="{{route('admin.category.primary.save')}}"

                          method="POST" enctype="multipart/form-data"

                          class="form-horizontal">


                        {{ csrf_field() }}

                        <div class="form-body">


                            <div class="card-body">
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Name</label>
                                            <div class="col-md-9">
                                                <input type="text" required name="title" class="form-control category-name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Description</label>
                                            <div class="col-md-9">
                                                <input type="text" required name="description" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Slug</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly required name="slug" class="form-control category-slug">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Featured</label>
                                            <div class="custom-control custom-radio">
                                               <select name="featured" class="form-control">
                                                   <option value="no">No</option>
                                                   <option value="yes">Yes</option>
                                               </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">



                                                <label class="control-label text-right col-md-3">Color</label>
                                                <div class="custom-control ">
                                                    <input type="color" name="color"  class="" value="#ff0000">

                                                </div>





                                        </div>
                                    </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Upload image</label>
                                                <div class="custom-control custom-radio">
                                                    <input name="image" type="file" class="form-control">
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                </div>
                                <!--/row-->

                                <!--/row-->
                            </div>
                            <hr>
                            <div class="form-actions">
                                <div class="card-body">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                        <button type="reset" class="btn btn-dark">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Row -->

    </div>

@endsection


@section('admin_footer')




    <script src="{{asset('../admin/assets/libs/jquery-asColor/dist/jquery-asColor.min.js')}}"></script>
    <script src="{{asset('../admin/assets/libs/jquery-asGradient/dist/jquery-asGradient.js')}}"></script>
    <script src="{{asset('../admin/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js')}}"></script>
    <script src="{{asset('../admin/assets/libs/@claviska/jquery-minicolors/jquery.minicolors.min.js')}}"></script>



    <script>



        $(document).ready(function(){
            $(".category-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".category-slug").val(actualSlug);



            });
        });


    </script>

    <script>
        $('.demo').each(function() {
            //
            // Dear reader, it's actually very easy to initialize MiniColors. For example:
            //
            //  $(selector).minicolors();
            //
            // The way I've done it below is just for the demo, so don't get confused
            // by it. Also, data- attributes aren't supported at this time...they're
            // only used for this demo.
            //
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                defaultValue: $(this).attr('data-defaultValue') || '',
                format: $(this).attr('data-format') || 'hex',
                keywords: $(this).attr('data-keywords') || '',
                inline: $(this).attr('data-inline') === 'true',
                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                opacity: $(this).attr('data-opacity'),
                position: $(this).attr('data-position') || 'bottom left',
                swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
    </script>


    @endsection


