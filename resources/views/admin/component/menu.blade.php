<!-- Sidebar navigation-->
<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="nav-small-cap">
            <i class="mdi mdi-dots-horizontal"></i>
            <span class="hide-menu">Personal</span>
        </li>



        <li class="sidebar-item">
            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <i class="mdi mdi-notification-clear-all"></i>
                <span class="hide-menu">Category</span>
            </a>
            <ul aria-expanded="false" class="collapse first-level">

                <li class="sidebar-item">
                    <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-playlist-plus"></i>
                        <span class="hide-menu">Primary</span>
                    </a>
                    <ul aria-expanded="false" class="collapse second-level">
                        <li class="sidebar-item">
                            <a href="{{route('admin.category.primary.create')}}" class="sidebar-link">
                                <i class="mdi mdi-octagram"></i>
                                <span class="hide-menu">Create </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('admin.category.primary.all')}}" class="sidebar-link">
                                <i class="mdi mdi-octagram"></i>
                                <span class="hide-menu"> View </span>
                            </a>
                        </li>

                    </ul>
                </li>



                <li class="sidebar-item">
                    <a class="has-arrow sidebar-link" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-playlist-plus"></i>
                        <span class="hide-menu">Secondary</span>
                    </a>
                    <ul aria-expanded="false" class="collapse second-level">
                        <li class="sidebar-item">
                            <a href="{{route('admin.category.secondary.create')}}" class="sidebar-link">
                                <i class="mdi mdi-octagram"></i>
                                <span class="hide-menu"> Create</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{route('admin.category.secondary.all')}}" class="sidebar-link">
                                <i class="mdi mdi-octagram"></i>
                                <span class="hide-menu"> View</span>
                            </a>
                        </li>

                    </ul>
                </li>

            </ul>
        </li>


        <li class="sidebar-item">
            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <i class="mdi mdi-av-timer"></i>
                <span class="hide-menu">Product </span>
                <span class="badge badge-pill badge-info ml-auto m-r-15">3</span>
            </a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item">
                    <a href="{{route('admin.product.primary.create')}}" class="sidebar-link">
                        <i class="mdi mdi-adjust"></i>
                        <span class="hide-menu"> Create </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a href="{{route('admin.product.primary.all')}}" class="sidebar-link">
                        <i class="mdi mdi-adjust"></i>
                        <span class="hide-menu"> View </span>
                    </a>
                </li>

            </ul>
        </li>

    </ul>
</nav>
<!-- End Sidebar navigation -->