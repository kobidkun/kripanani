<nav class="navbar navbar-primary navbar-full">
    <div class="container">
        <ul class="nav navbar-nav departments-menu animate-dropdown">
            <li class="nav-item dropdown ">

                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle" >Shop by Department</a>
                <ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown">
                    <li class="highlight menu-item animate-dropdown active"><a title="Value of the Day" href="product-category.html">Today's Deal</a></li>
                    <li class="highlight menu-item animate-dropdown"><a title="Today's Offers" href="home-v3.html">Today's Offers</a></li>
                    <li class="highlight menu-item animate-dropdown"><a title="Just Arrived" href="home-v3-full-color-background.html">Just Arrived</a></li>

                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                        <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown"
                           class="dropdown-toggle" aria-haspopup="true">Dal & Pulses</a>
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                <div class="yamm-content">

                                    <div class="vc_row row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                <li><a href="#">Computer Accessories</a></li>
                                                                <li><a href="#">Software</a></li>
                                                                <li class="nav-divider"></li>
                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Office &amp; Stationery</li>
                                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                                <li><a href="#">Pens &amp; Writing</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                        <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown"
                           class="dropdown-toggle" aria-haspopup="true">Oil & Ghee</a>
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                <div class="yamm-content">
                                    <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                <li><a href="#">Computer Accessories</a></li>
                                                                <li><a href="#">Software</a></li>
                                                                <li class="nav-divider"></li>
                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Office &amp; Stationery</li>
                                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                                <li><a href="#">Pens &amp; Writing</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2586 dropdown">
                        <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle"
                           aria-haspopup="true">Washing powder</a>
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                <div class="yamm-content">
                                    <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                <li><a href="#">Computer Accessories</a></li>
                                                                <li><a href="#">Software</a></li>
                                                                <li class="nav-divider"></li>
                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Office &amp; Stationery</li>
                                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                                <li><a href="#">Pens &amp; Writing</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>


                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2587 dropdown">
                        <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown"
                           class="dropdown-toggle" aria-haspopup="true">Soaps</a>
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                <div class="yamm-content">
                                    <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                <li><a href="#">Computer Accessories</a></li>
                                                                <li><a href="#">Software</a></li>
                                                                <li class="nav-divider"></li>
                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Office &amp; Stationery</li>
                                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                                <li><a href="#">Pens &amp; Writing</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>





                </ul>
            </li>
        </ul>
        <form class="navbar-search" method="get" action="/">
            <label class="sr-only screen-reader-text" for="search">Search for:</label>
        </form>

        <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
            <li class="nav-item dropdown">
                <a href="cart.html" class="nav-link" data-toggle="dropdown">
                    <i class="ec ec-shopping-bag"></i>
                    <span class="cart-items-count count">4</span>
                    <span class="cart-items-total-price total-price"><span class="amount">&#36;1,215.00</span></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-mini-cart">
                    <li>
                        <div class="widget_shopping_cart_content">

                            <ul class="cart_list product_list_widget ">


                                <li class="mini_cart_item">
                                    <a title="Remove this item" class="remove" href="#">×</a>
                                    <a href="single-product.html">
                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
                                    </a>

                                    <span class="quantity">2 × <span class="amount">£150.00</span></span>
                                </li>


                                <li class="mini_cart_item">
                                    <a title="Remove this item" class="remove" href="#">×</a>
                                    <a href="single-product.html">
                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
                                    </a>

                                    <span class="quantity">1 × <span class="amount">£399.99</span></span>
                                </li>

                                <li class="mini_cart_item">
                                    <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
                                    <a href="single-product.html">
                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

                                    </a>

                                    <span class="quantity">1 × <span class="amount">£269.99</span></span>
                                </li>


                            </ul><!-- end product list -->


                            <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                            <p class="buttons">
                                <a class="button wc-forward" href="cart.html">View Cart</a>
                                <a class="button checkout wc-forward" href="checkout.html">Checkout</a>
                            </p>


                        </div>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="navbar-wishlist nav navbar-nav pull-right flip">
            <li class="nav-item">
                <a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
            </li>
        </ul>
        <ul class="navbar-compare nav navbar-nav pull-right flip">
            <li class="nav-item">
                <a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
            </li>
        </ul>
    </div>
</nav>
