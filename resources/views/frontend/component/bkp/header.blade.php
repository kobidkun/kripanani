<header id="masthead" class="site-header header-v2">
    <div class="container">
        <div class="row">


            <style>
                .header-logo-image{
                    width: 180px;
                }
            </style>


            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">
                    <img class="header-logo-image" src="{{asset('image/logo/logo.png')}}" alt="">
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="primary-nav animate-dropdown">
                <div class="clearfix">
                    <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                        &#9776;
                    </button>
                </div>

                <div class="collapse navbar-toggleable-xs" id="default-header">
                    <nav>
                        <ul id="menu-main-menu" class="nav nav-inline yamm">

                            <li class="menu-item animate-dropdown"><a title="About Us" href="about.html">Home</a></li>
                            <li class="menu-item animate-dropdown"><a title="About Us" href="about.html">About Us</a></li>
                            <li class="menu-item animate-dropdown"><a title="About Us" href="about.html">Contact Us</a></li>

                            
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="header-support-info">
                <div class="media">
                    <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
                    <div class="media-body">
                        <span class="support-number"><strong>Support</strong> (+800) 856 800 604</span><br/>
                        <span class="support-email">Email: info@electro.com</span>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div>
</header><!-- #masthead -->
