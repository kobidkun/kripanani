<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->text('txt_id');
            $table->text('order_id');
            $table->text('secure_id');
            $table->text('taxable_value');
            $table->text('tax_amount');
            $table->text('cgst_amt');
            $table->text('sgst_amt');
            $table->text('igst_amt');
            $table->text('total');
            $table->text('customer_id');
            $table->text('billing_address');
            $table->text('delivery_address');
            $table->text('shipping_type');
            $table->text('payment_type');
            $table->text('status');
            $table->text('delivery_note');
            $table->text('user_ip');
            $table->text('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
