<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->longText('description');
            $table->text('img')->nullable();
            $table->text('img_small')->nullable();
            $table->text('img_hd')->nullable();
            $table->text('meta')->nullable();
            $table->text('slug')->nullable();
            $table->text('hsn');
            $table->text('primary_category');
            $table->text('primary_category_name');
            $table->text('secondary_category');
            $table->text('secondary_category_name');
            $table->text('code_name')->nullable();
            $table->text('type')->nullable();
            $table->text('unit')->nullable();
            $table->text('value');
            $table->text('discount')->nullable();
            $table->text('discount_per')->nullable();
            $table->text('after_discount_amt')->nullable();
            $table->text('cgst');
            $table->text('sgst');
            $table->text('igst');
            $table->text('cgst_per');
            $table->text('sgst_per');
            $table->text('igst_per');
            $table->text('taxper')->nullable();
            $table->text('tax_amt')->nullable();
            $table->text('total');
            $table->string('featured')->default('0');
            $table->boolean('active')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
