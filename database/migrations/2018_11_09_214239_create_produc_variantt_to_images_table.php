<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducVarianttToImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produc_variantt_to_images', function (Blueprint $table) {
            $table->increments('id');
            $table->text('product_vatiant_id');
            $table->text('img');
            $table->text('img_small');
            $table->text('img_hd');
            $table->text('meta');
            $table->text('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produc_variantt_to_images');
    }
}
