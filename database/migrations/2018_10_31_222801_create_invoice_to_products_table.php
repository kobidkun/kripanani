<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_to_products', function (Blueprint $table) {
            $table->text('id');
            $table->text('order_id');
            $table->text('product_vatiant_id');
            $table->text('product_id');
            $table->text('invoice_id');
            $table->text('vatiant');
            $table->text('title');
            $table->text('slug')->nullable();
            $table->text('hsn');
            $table->text('code_name')->nullable();
            $table->text('type')->nullable();
            $table->text('unit')->nullable();
            $table->text('value');
            $table->text('discount')->nullable();
            $table->text('discount_per')->nullable();
            $table->text('cgst');
            $table->text('sgst');
            $table->text('igst');
            $table->text('cgst_per');
            $table->text('sgst_per');
            $table->text('igst_per');
            $table->text('taxper')->nullable();
            $table->text('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_to_products');
    }
}
