<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function order_to_products()
    {
        return $this->hasMany('App\Model\Order\OrderToProduct');
    }
}
