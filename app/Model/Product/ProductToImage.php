<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductToImage extends Model
{
    public function products(){
        return $this->belongsTo('App\Model\Product\Product','id','product_id');
    }
}
