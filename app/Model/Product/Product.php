<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function product_vatiants()
    {
        return $this->hasMany('App\Model\Product\ProductVatiant');
    }




    public function product_to_features()
    {
        return $this->hasMany('App\Model\Product\ProductToFeatures');
    }
}
