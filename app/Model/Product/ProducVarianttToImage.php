<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProducVarianttToImage extends Model
{
    public function product_vatiants(){
        return $this->belongsTo('App\Model\Product\ProductVatiant','id','product_id');
    }
}
