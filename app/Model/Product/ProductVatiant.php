<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductVatiant extends Model
{
    public function produc_variantt_to_images()
    {
        return $this->hasMany('App\Model\Product\ProducVarianttToImage');
    }

    public function product_variant_to_features()
    {
        return $this->hasMany('App\Model\Product\ProductVariantToFeatures');
    }
}
