<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Product\Product;
use App\Model\Product\ProductToImage;
use App\Model\Product\ProductVatiant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
   public function HomePage(Request $request){

       $latestproduct = Product::orderBy('id', 'desc')->take(5)->get();

       $featuredproduct = Product::where('featured', 1)->orderBy('id', 'desc')->take(5)->get();


       return view('frontend.page.home')->with([
           'latestproducts' => $latestproduct,
           'latests' => $latestproduct,
           'featureds' => $featuredproduct,

       ]);
   }

   public function DetailsPage($slug ){

       $product = Product::where('slug', $slug)->first();

       $images = ProductToImage::where('id', '1')->get();

       $provar = ProductVatiant::where('product_id', $product->id)->get();



      // dd($product);

       return view('frontend.page.productdetails')->with([
           'product' => $product,
           'vars' => $provar,
           'images' => $images,

       ]);




   }
}
