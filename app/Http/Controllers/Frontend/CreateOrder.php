<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Order\Order;
use App\Model\Product\ProductVatiant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CreateOrder extends Controller
{
    public function SaveOrderLoggedIn(Request $request){


        $createsecureorderid = ('OD' . $this->random_str('alphanum', 10));

        $customer = Auth::user();

        $saveorder = new Order();

        $saveorder->txt_id = $request->txt_id;
        $saveorder->secure_id = $request->txt_id;
        $saveorder->txt_id = $createsecureorderid;
        $saveorder->customer_id = $request->txt_id;
        $saveorder->billing_address = $request->txt_id;
        $saveorder->delivery_address = $request->txt_id;
        $saveorder->shipping_type = $request->txt_id;
        $saveorder->payment_type = $request->txt_id;
        $saveorder->user_ip = $request->ip();
        $saveorder->date = time();

        $saveorder->save();







        $products = $request->input('product');

        foreach ($products as $key => $value) {

            $findproduct = ProductVatiant::find($value->id)->first();

            $saveorder->order_to_products()->create([



                'order_id' => $saveorder->id,
                'product_id' => $findproduct->id,
                'parent_product_id' => $findproduct->product_id,
                'customer_id' => $customer->id,
                'vatiant' => $value['color'],
                'size' => $value['size'],
                'title' => $value['product_name'],
                'slug' => $value['quantity'],
                'quantity' => $value['quantity'],
                'hsn' => $value['hsn'],
                'code_name' => $value['type'],
                'type' => $value['type'],
                'unit' => $value['type'],
                'taxable_rate' => $value['taxable_rate'],
                'taxable_value' => $value['taxable_value'],
                'taxable_discount_amount' => $value['taxable_discount_amount'],
                'taxable_tax_cgst' => $value['taxable_tax_cgst'],
                'taxable_tax_cgst_percentage' => $value['taxable_tax_cgst_percentage'],
                'taxable_tax_sgst' => $value['taxable_tax_sgst'],
                'taxable_tax_sgst_percentage' => $value['taxable_tax_sgst_percentage'],
                'taxable_tax_igst' => $value['taxable_tax_igst'],
                'taxable_tax_igst_percentage' => $value['taxable_tax_igst_percentage'],
                'taxable_tax_cess' => $value['taxable_tax_cess'],
                'taxable_tax_cess_percentage' => $value['taxable_tax_cess_percentage'],
                'taxable_tax_total' => $value['taxable_tax_total'],
                'total' => $value['total'],


            ]);
        }









    }


    function random_str($type = 'alphanum', $length = 10)
    {
        switch ($type) {
            case 'basic'    :
                return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings = array();
                $seedings['alpha'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num'] = '0123456789';
                $seedings['nozero'] = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }
}
