<?php

namespace App\Http\Controllers\Admin\Product\PrimaryProduct;

use App\Http\Requests\Product\CreateProductValidation;
use App\Model\Category\PrimaryCategory;
use App\Model\Category\SecondaryCategory;
use App\Model\Product\Product;
use App\Model\Product\ProductToFeatures;
use App\Model\Product\ProductToImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManagePrimaryProduct extends Controller
{
    public function CreatePrimary(){

        $pc = PrimaryCategory::all();

        $sc = SecondaryCategory::all();


        return view('admin.pages.product.product.create')->with([
            'pcs' => $pc,
            'scs' => $sc
        ]);
    }


    public function SaveProduct(CreateProductValidation $request){

        if ( $request->imgsm  === null ){
            return 'no Small image ';
        } else if ( $request->imglg  === null ){
            return 'no HD image ';
        }

        else if ( $request->imgssm  === null ){
            return 'no Xtra Small  image ';
        }



        $imglocname =  time().'-'.$request->imglg->getClientOriginalName();
        $imglocnamepath = '/images/product/hd/'.$imglocname;

        $imgloc = $request->imglg->storeAs('public/images/product/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->imgsm->getClientOriginalName();
        $imglocsmnamepath = '/images/product/sm/'.$imglocsmname;
        $imglocsm = $request->imgsm->storeAs('public/images/product/sm',$imglocsmname);


        $imglocssmname = time().'-'.$request->imgssm->getClientOriginalName();
        $imglocssmnamepath = '/images/product/ssm/'.$imglocssmname;
        $imglocssm = $request->imgssm->storeAs('public/images/product/ssm', $imglocssmname);




        $a = new Product();

        $a->title = $request->title;
        $a->description = $request->description;
        $a->meta = $request->meta;
        $a->slug = time().'-'.$request->slug;
        $a->hsn = $request->hsn;
        $a->primary_category = $request->primary_category;
        $a->primary_category_name = $request->primary_category_name;
        $a->secondary_category_name = $request->secondary_category_name;
        $a->secondary_category = $request->secondary_category;
        $a->code_name = $request->code_name;
        $a->type = $request->type;
        $a->unit = $request->unit;
        $a->value = $request->value;
        $a->discount = $request->discount;
        $a->discount_per = $request->discount_per;
        $a->after_discount_amt = $request->after_discount_amt;
        $a->cgst = $request->cgst;
        $a->sgst = $request->sgst;
        $a->igst = $request->igst;
        $a->cgst_per =($request->taxper/2);
        $a->sgst_per = ($request->taxper/2);
        $a->igst_per = $request->taxper;
        $a->taxper = $request->taxper;
        $a->tax_amt = $request->tax_amt;
        $a->total = $request->total;
        $a->featured = '0';
        $a->img = $imglocssmnamepath;
        $a->img_small = $imglocsmnamepath;
        $a->img_hd = $imglocnamepath;
        $a->active = '1';

        $a->save();

        return back();




    }

    public function ViewAll(){
        return view('admin.pages.product.product.all');
    }

    public function ViewDetails($id){

        $a = Product::find($id);

        $b = ProductToFeatures::where('product_id', $id)->get();

        return view('admin.pages.product.product.details')->with([
            'p' => $a,
            'fs' => $b

            ]);
    }


    public function DatatableApi(){



        $users = Product::select([
            'id',
            'title',
            'code_name',
            'type',
            'value',
            'discount',
            'taxper',
            'tax_amt',
            'total',
            'featured',
            'updated_at'
        ]);

        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.product.primary.details',$user->id).'"></i> Details</a>';
            })

            ->make(true);






    }

    public function UploadImages($id, Request $request){


        if ( $request->file  === null ){
            return 'no Small image ';
        }

    // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/product/details/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/product/details', $imglocname);


        $a = new ProductToImage();

        $a->products_id = $id;
        $a->img = $imglocnamepath;
        $a->img_small = $imglocnamepath;
        $a->img_hd = $imglocnamepath;
        $a->meta = $imglocname;
        $a->title = $imglocname;

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);


    }


    public function AddFeatures(Request $request){
        $a = new ProductToFeatures();
        $a->product_id = $request->product_id;
        $a->feature = $request->feature;
        $a->save();
        return back();
    }


}
