<?php

namespace App\Http\Controllers\Admin\Product\ProductVariants;

use App\Model\Product\Product;
use App\Model\Product\ProductVariantToFeatures;
use App\Model\Product\ProductVatiant;
use App\Model\Product\ProducVarianttToImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CreateProductVariants extends Controller
{
    public function CreatePrimary($id){


        $fprod = Product::findorfail($id);

        //dd($fprod);



        return view('admin.pages.product.productvariant.create')->with([

            'p' => $fprod
        ]);
    }


    public function SaveProduct(Request $request){



        $pro = Product::find($request->proid);



        $a = new ProductVatiant();

        $a->product_id = $request->proid;
        $a->title = $request->title;
        $a->description = $request->description;
        $a->meta = $request->meta;
        $a->slug = time().'-'.$request->slug;
        $a->hsn = $request->hsn;
        $a->primary_category = $pro->primary_category;
        $a->primary_category_name = $pro->primary_category_name;
        $a->secondary_category_name = $pro->secondary_category_name;
        $a->secondary_category = $pro->secondary_category;
        $a->code_name = $pro->code_name;
        $a->type = $pro->type;
        $a->unit = $pro->unit;
        $a->value = $pro->value;
        $a->size = $request->size;
        $a->discount = $request->discount;
        $a->discount_per = $request->discount_per;
        $a->after_discount_amt = $request->after_discount_amt;
        $a->cgst = $request->cgst;
        $a->sgst = $request->sgst;
        $a->igst = $request->igst;
        $a->cgst_per =($request->taxper/2);
        $a->sgst_per = ($request->taxper/2);
        $a->igst_per = $request->taxper;
        $a->taxper = $request->taxper;
        $a->tax_amt = $request->tax_amt;
        $a->total = $request->total;
        $a->featured = '0';
        $a->img = $pro->img;
        $a->img_small = $pro->img_small;
        $a->img_hd = $pro->img_hd;
        $a->active = '1';

        $a->save();

        return back();




    }

    public function ViewAll(){
        return view('admin.pages.product.product.all');
    }

    public function ViewDetails($id){

        $a = ProductVatiant::find($id);


        $b = ProductVariantToFeatures::where('product_id', $id)->get();





        return view('admin.pages.product.productvariant.details')->with([
            'p' => $a,
            'fs' => $b

        ]);
    }


    public function DatatableApi($id){



        $users = ProductVatiant::where('product_id', $id)->select([
            'id',
            'title',
            'code_name',
            'type',
            'value',
            'discount',
            'taxper',
            'tax_amt',
            'total',
            'featured',
            'updated_at'
        ]);

        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.product.variants.details',$user->id).'"></i> Details</a>';
            })

            ->make(true);






    }


    public function UploadImages($id, Request $request){


        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/product/details-variant/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/product-variant/details', $imglocname);


        $a = new ProducVarianttToImage();

        $a->product_vatiant_id = $id;
        $a->img = $imglocnamepath;
        $a->img_small = $imglocnamepath;
        $a->img_hd = $imglocnamepath;
        $a->meta = $imglocname;
        $a->title = $imglocname;

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);


    }


    public function AddFeatures(Request $request){
        $a = new ProductVariantToFeatures();
        $a->product_id = $request->product_id;
        $a->feature = $request->feature;
        $a->save();
        return back();
    }

}
