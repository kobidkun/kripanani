<?php

namespace App\Http\Controllers\Admin\Category\Secondary;

use App\Model\Category\PrimaryCategory;
use App\Model\Category\SecondaryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageSecondaryCategory extends Controller
{
    public function CreateSecondary(){

        $a = PrimaryCategory::all();

        return view('admin.pages.category.secondary.create')->with('pcs' , $a);
    }

    public function SaveSecondary(Request $request){



        if ($request->image != null ){
            $filelocation =  $request->image->storeAs('images/category/primary', time().'-'.$request->image->getClientOriginalName());

            $a = new SecondaryCategory();
            $a->title = $request->title;
            $a->description = $request->description;
            $a->color = $request->color;
            $a->slug = $request->slug;
            $a->featured = $request->featured;
            $a->img_url = $filelocation;
            $a->primary_category_id = $request->primary_category_id;

            $a->save();

        } else {

            $a = new SecondaryCategory();
            $a->title = $request->title;
            $a->description = $request->description;
            $a->color = $request->color;
            $a->slug = $request->slug;
            $a->featured = $request->featured;
            $a->primary_category_id = $request->primary_category_id;
            //   $a->img_url = $filelocation;

            $a->save();


        }

        return back();



    }


    public function AllSecondary(){
        $a = SecondaryCategory::all();

        return view('admin.pages.category.secondary.all')->with('categories', $a);
    }
}
