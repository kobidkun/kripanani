<?php

namespace App\Http\Controllers\Admin\Category\Primary;

use App\Model\Category\PrimaryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ManagePrimaryCategory extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function CreatePrimary(){
        return view('admin.pages.category.primary.create');
    }

    public function SavePrimary(Request $request){



     // dd($filelocation);


        if ($request->image != null ){
            $filelocation =  $request->image->storeAs('images/category/primary', time().'-'.$request->image->getClientOriginalName());

            $a = new PrimaryCategory();
            $a->title = $request->title;
            $a->description = $request->description;
            $a->color = $request->color;
            $a->slug = $request->slug;
            $a->featured = $request->featured;
            $a->img_url = $filelocation;

            $a->save();

        } else {

            $a = new PrimaryCategory();
            $a->title = $request->title;
            $a->description = $request->description;
            $a->color = $request->color;
            $a->slug = $request->slug;
            $a->featured = $request->featured;
         //   $a->img_url = $filelocation;

            $a->save();


        }




        return back();



    }


    public function AllPrimary(){
        $a = PrimaryCategory::all();

        return view('admin.pages.category.primary.all')->with('categories', $a);
    }


}
