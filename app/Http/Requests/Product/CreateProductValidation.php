<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'slug' => 'required|unique:products',
            /*'img' => 'required',
            'img_small' => 'required',
            'img_hd' => 'required',*/
            'hsn' => 'required',
            'code_name' => 'required',
            'type' => 'required',
            'unit' => 'required',
            'value' => 'required',
            'total' => 'required'
        ];
    }
}
