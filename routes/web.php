<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/cart', function () {
    return view('frontend.page.cart');
});

Route::get('/checkout', function () {
    return view('frontend.page.checkout');
});






Route::get('/', 'Frontend\HomepageController@HomePage')
    ->name('frontend.homepage');

Route::get('/details/{slug}', 'Frontend\HomepageController@DetailsPage')
    ->name('frontend.details');










//admin routes

//cat routes

Route::get('admin/category/primary/create', 'Admin\Category\Primary\ManagePrimaryCategory@CreatePrimary')
    ->name('admin.category.primary.create');

Route::post('admin/category/primary/create', 'Admin\Category\Primary\ManagePrimaryCategory@SavePrimary')
    ->name('admin.category.primary.save');

Route::get('admin/category/primary/all', 'Admin\Category\Primary\ManagePrimaryCategory@AllPrimary')
    ->name('admin.category.primary.all');



Route::get('admin/category/secondary/create', 'Admin\Category\Secondary\ManageSecondaryCategory@CreateSecondary')
    ->name('admin.category.secondary.create');

Route::post('admin/category/secondary/create', 'Admin\Category\Secondary\ManageSecondaryCategory@SaveSecondary')
    ->name('admin.category.secondary.save');

Route::get('admin/category/secondary/all', 'Admin\Category\Secondary\ManageSecondaryCategory@AllSecondary')
    ->name('admin.category.secondary.all');



//product

Route::get('admin/product/primary/create', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@CreatePrimary')
    ->name('admin.product.primary.create');


Route::get('admin/product/primary', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@ViewAll')
    ->name('admin.product.primary.all');


Route::get('admin/product/details/{id}', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@ViewDetails')
    ->name('admin.product.primary.details');

Route::get('admin/product/primary/datatablesapi', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@DatatableApi')
    ->name('admin.product.primary.datatablesapi');

Route::post('admin/product/primary/create', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@SaveProduct')
    ->name('admin.product.primary.save');


Route::post('admin/product/primary/upload/image/{id}', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@UploadImages')
    ->name('admin.product.primary.upload.image');


Route::post('admin/product/primary/add/features', 'Admin\Product\PrimaryProduct\ManagePrimaryProduct@AddFeatures')
    ->name('admin.product.primary.add.feature');



Route::get('admin/product/variants/create/{id}', 'Admin\Product\ProductVariants\CreateProductVariants@CreatePrimary')
    ->name('admin.product.variants.create');


Route::get('admin/product/variants', 'Admin\Product\ProductVariants\CreateProductVariants@ViewAll')
    ->name('admin.product.variants.all');


Route::get('admin/product/variants/{id}', 'Admin\Product\ProductVariants\CreateProductVariants@ViewDetails')
    ->name('admin.product.variants.details');

Route::get('admin/product/variants/datatablesapi/{id}', 'Admin\Product\ProductVariants\CreateProductVariants@DatatableApi')
    ->name('admin.product.variants.datatablesapi');

Route::post('admin/product/variants/create', 'Admin\Product\ProductVariants\CreateProductVariants@SaveProduct')
    ->name('admin.product.variants.save');


Route::post('admin/product/variants/upload/image/{id}', 'Admin\Product\ProductVariants\CreateProductVariants@UploadImages')
    ->name('admin.product.variants.upload.image');


Route::post('admin/product/variants/add/features', 'Admin\Product\ProductVariants\CreateProductVariants@AddFeatures')
    ->name('admin.product.variants.add.feature');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




///admin auth routes
///
    Route::prefix('admin')->group(function() {
    Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/home', 'Admin\Auth\AdminController@index')->name('admin.home');
});
